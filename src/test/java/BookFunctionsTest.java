import bookstore.constructor.Book;
import bookstore.functions.BookFunctions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookFunctionsTest {


    //<editor-fold desc="Lists for tests">
    List<Book> listBooks() {
        List<Book> books = new ArrayList<Book>();
        books.add(new Book(1, "Clean Code", "132350882", 2008, 'T', null, null));
        books.add(new Book(2, "Effective Java (3rd Edition)", "134685997", 2018, 'M', null, null));
        books.add(new Book(3, "Test Driven Development: By Example", "321146530", 2002, 'M', null, null));
        books.add(new Book(4, "Patterns of Enterprise Application Architecture", "321127420", 2002, 'T', null, null));
        books.add(new Book(5, "Head First Design Patterns", "596007124", 2004, 'M', null, null));
        books.add(new Book(6, "Clean Architecture", "134494164", 2017, 'M', null, null));

        return books;
    }

    List<Book> listBooksAlphabetically() {
        List<Book> books = new ArrayList<Book>();
        books.add(new Book(6, "Clean Architecture", "134494164", 2017, 'M', null, null));
        books.add(new Book(1, "Clean Code", "132350882", 2008, 'T', null, null));
        books.add(new Book(2, "Effective Java (3rd Edition)", "134685997", 2018, 'M', null, null));
        books.add(new Book(5, "Head First Design Patterns", "596007124", 2004, 'M', null, null));
        books.add(new Book(4, "Patterns of Enterprise Application Architecture", "321127420", 2002, 'T', null, null));
        books.add(new Book(3, "Test Driven Development: By Example", "321146530", 2002, 'M', null, null));

        return books;
    }

    List<Book> divisibleBy2() {
        List<Book> books = new ArrayList<Book>();
        books.add(new Book(1, "Clean Code", "132350882", 2008, 'T', null, null));
        books.add(new Book(2, "Effective Java (3rd Edition)", "134685997", 2018, 'M', null, null));
        books.add(new Book(3, "Test Driven Development: By Example", "321146530", 2002, 'M', null, null));
        books.add(new Book(4, "Patterns of Enterprise Application Architecture", "321127420", 2002, 'T', null, null));
        books.add(new Book(5, "Head First Design Patterns", "596007124", 2004, 'M', null, null));


        return books;
    }

    List<Book> listBooks100() {
        List<Book> books = new ArrayList<Book>();
        books.add(new Book(1, "Clean Code", "132350882", 2108, 'T', null, null));
        books.add(new Book(2, "Effective Java (3rd Edition)", "134685997", 2118, 'M', null, null));
        books.add(new Book(3, "Test Driven Development: By Example", "321146530", 2102, 'M', null, null));
        books.add(new Book(4, "Patterns of Enterprise Application Architecture", "321127420", 2102, 'T', null, null));
        books.add(new Book(5, "Head First Design Patterns", "596007124", 2104, 'M', null, null));
        books.add(new Book(6, "Clean Architecture", "134494164", 2117, 'M', null, null));

        return books;
    }

    List<Book> listBooksSortedAscendingOrder() {
        List<Book> books = new ArrayList<Book>();
        books.add(new Book(3, "Test Driven Development: By Example", "321146530", 2002, 'M', null, null));
        books.add(new Book(4, "Patterns of Enterprise Application Architecture", "321127420", 2002, 'T', null, null));
        books.add(new Book(5, "Head First Design Patterns", "596007124", 2004, 'M', null, null));
        books.add(new Book(1, "Clean Code", "132350882", 2008, 'T', null, null));
        books.add(new Book(6, "Clean Architecture", "134494164", 2017, 'M', null, null));
        books.add(new Book(2, "Effective Java (3rd Edition)", "134685997", 2018, 'M', null, null));

        return books;
    }

    List<Book> listBooksSortedDescendingOrder() {
        List<Book> books = new ArrayList<Book>();
        books.add(new Book(2, "Effective Java (3rd Edition)", "134685997", 2018, 'M', null, null));
        books.add(new Book(6, "Clean Architecture", "134494164", 2017, 'M', null, null));
        books.add(new Book(1, "Clean Code", "132350882", 2008, 'T', null, null));
        books.add(new Book(5, "Head First Design Patterns", "596007124", 2004, 'M', null, null));
        books.add(new Book(3, "Test Driven Development: By Example", "321146530", 2002, 'M', null, null));
        books.add(new Book(4, "Patterns of Enterprise Application Architecture", "321127420", 2002, 'T', null, null));

        return books;
    }
    //</editor-fold>


    @Test
    void isISBNMatches() {
        final List<Book> books = listBooks();
        BookFunctions bookFunctions = new BookFunctions();
        final Book testCase = bookFunctions.findByISBN(books, "132350882");
        final Book testCaseStream = bookFunctions.findByISBNStreamStyle(books, "132350882");
        final Book expectedBook = new Book(1, "Clean Code", "132350882", 2008, 'T', null, null);
        assertEquals(expectedBook, testCase);
        assertEquals(expectedBook, testCaseStream);
    }

    @Test
    void isISBNWrong() {
        final BookFunctions bookFunctions = new BookFunctions();
        bookFunctions.findByISBN(listBooks(), "0000000");

    }

    @Test
    void isSum() {
        BookFunctions bookFunctions = new BookFunctions();
        final int testCase = bookFunctions.yaerSum(listBooks());
        final int testCaseStream = bookFunctions.yaerSumStreamStyle(listBooks());
        assertEquals(12051, testCase);
        assertEquals(12051, testCaseStream);
    }

    @Test
    void isAfter2007() {
        BookFunctions bookFunctions = new BookFunctions();
        final int testCase = bookFunctions.bookCountAfter2007(listBooks());
        final int testCaseStream = bookFunctions.bookCountAfter2007StreamStyle(listBooks());
        assertEquals(3, testCase);
        assertEquals(3, testCaseStream);
    }

    @Test
    void isAllBooksAfter2000() {
        BookFunctions bookFunctions = new BookFunctions();
        boolean testCase = bookFunctions.isAfter2000(listBooks());
        boolean testCaseStream = bookFunctions.isAfter2000StreamStyle(listBooks());
        assertTrue(testCase);
        assertTrue(testCaseStream);
    }

    @Test
    void isReturnTwoLastBooks() {
        BookFunctions bookFunctions = new BookFunctions();
        List<Book> testCase = bookFunctions.giveMeTwoLastBooks(listBooks());
        assertEquals(5, testCase.get(0).getNumber());
        assertEquals(6, testCase.get(1).getNumber());

        List<Book> testCaseStream = bookFunctions.giveMeTwoLastBooksStreamStyle(listBooks());
        assertEquals(5, testCaseStream.get(0).getNumber());
        assertEquals(6, testCaseStream.get(1).getNumber());
    }

    @Test
    void isEarliestRelease() {
        BookFunctions bookFunctions = new BookFunctions();
        Book testCase = bookFunctions.givingEarliestRelease(listBooks());
        Book testCaseStream = bookFunctions.givingEarliestReleaseStreamStyle(listBooks());

        assertEquals(3, testCase.getNumber());
        assertEquals(3, testCaseStream.getNumber());
    }

    @Test
    void isLatestRelease() {
        BookFunctions bookFunctions = new BookFunctions();
        List<Book> testCase = bookFunctions.givingLatestrelease(listBooks());
        List<Book> testCaseStream = bookFunctions.givingLatestreleaseStreamStyle(listBooks());
        Book expected = new Book(2, "Effective Java (3rd Edition)", "134685997", 2018, 'M', null, null);
        assertEquals(expected, testCase.get(1));
        assertEquals(expected, testCaseStream.get(1));
    }

    @Test
    void isAverageYearRelease() {
        BookFunctions bookFunctions = new BookFunctions();
        double testCase = bookFunctions.givingAverageYearRelease(listBooks());
        double testCasestream = bookFunctions.givingAverageYearReleaseStreamStyle(listBooks());
        assertEquals(2008.5, testCase, 0.002);
        assertEquals(2008.5, testCasestream);

    }

    @Test
    void isReleasedBefore2003() {
        final BookFunctions bookFunctions = new BookFunctions();
        boolean testCase = bookFunctions.checkIfReleasedBefore2003(listBooks());
        boolean testCaseStream = bookFunctions.checkIfReleasedBefore2003StreamStyle(listBooks());
        assertEquals(true, testCase);
        assertEquals(true, testCaseStream);
    }

    @Test
    void isBooksStartsWithCAndReleasedAfter2010() {
        final BookFunctions bookFunctions = new BookFunctions();
        List<Book> testCase = bookFunctions.givingBooksStartsWithCAndReleasedAfter2010(listBooks());
        List<Book> testCaseStream = bookFunctions.givingBooksStartsWithCAndReleasedAfter2010streamStyle(listBooks());
        final Book expected = new Book(6, "Clean Architecture", "134494164", 2017, 'M', null, null);
        assertEquals(expected, testCase.get(1));
        assertEquals(expected, testCaseStream.get(1));
    }

    @Test
    void isSortedByReleaseDateAscending() {
        final BookFunctions bookFunctions = new BookFunctions();
        final List<Book> testCase = bookFunctions.returnSortedBookListByYearAscending(listBooks());
        final List<Book> testCaseStream = bookFunctions.returnSortedBookListByYearAscendingStreamStyle(listBooks());
        List<Book> expected = listBooksSortedAscendingOrder();
        Assertions.assertArrayEquals(new List[]{expected}, new List[]{testCase});
        Assertions.assertArrayEquals(new List[]{expected}, new List[]{testCaseStream});

    }

    @Test
    void isSortedByReleaseDateDescending() {
        final BookFunctions bookFunctions = new BookFunctions();
        List<Book> testCase = bookFunctions.returnSortedBookListByYearDescending(listBooks());
        List<Book> testCaseStream = bookFunctions.returnSortedBookListByYearDescendingStreamStyle(listBooks());
        List<Book> expected = listBooksSortedDescendingOrder();
        Assertions.assertArrayEquals(new List[]{expected}, new List[]{testCase});
        Assertions.assertArrayEquals(new List[]{expected}, new List[]{testCaseStream});
    }

    @Test
    void isAdded100YearsToReleaseDate() {
        final BookFunctions bookFunctions = new BookFunctions();
        List<Book> testCase = bookFunctions.adding100YearsToReleaseDate(listBooks());
        List<Book> testCaseStream = bookFunctions.adding100YearsToReleaseDateStreamStyle(listBooks());
        List<Book> expected = listBooks100();
        Assertions.assertArrayEquals(new List[]{expected}, new List[]{testCase});
        Assertions.assertArrayEquals(new List[]{expected}, new List[]{testCaseStream});
    }

    @Test
    void isYearDivisibleBy2() {
        final BookFunctions bookFunctions = new BookFunctions();
        List<Book> testCase = bookFunctions.booksDivisibleByTwo(listBooks());
        List<Book> testCaseStream = bookFunctions.booksDivisibleByTwoStreamStyle(listBooks());
        List<Book> expected = divisibleBy2();
        Assertions.assertArrayEquals(expected.toArray(), testCase.toArray());
        Assertions.assertArrayEquals(expected.toArray(), testCaseStream.toArray());
    }

    @Test
    void isSortedAlphabetically() {
        final BookFunctions bookFunctions = new BookFunctions();
        List<Book> testCase = bookFunctions.sortingAlphabetically(listBooks());
        List<Book> testCaseStream = bookFunctions.sortingAlphabeticallyStreamStyle(listBooks());
        List<Book> expected = listBooksAlphabetically();
        Assertions.assertArrayEquals(expected.toArray(), testCase.toArray());
        Assertions.assertArrayEquals(expected.toArray(), testCaseStream.toArray());
    }
}