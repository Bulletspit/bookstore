import bookstore.constructor.Book;
import bookstore.data.BookData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class BookDataTest {

    List<Book> listBooks() {
        List<Book> books = new ArrayList<Book>();
        books.add(new Book(4, "Patterns of Enterprise Application Architecture", "321127420", 2002, 'T', null, null));
        books.add(new Book(5, "Head First Design Patterns", "596007124", 2004, 'M', null, null));
        return books;
    }

    @Test
    void isPrintedByCategoryDesignPatterns() {
        BookData bookData = new BookData();
       // List<bookstore.constructor.Book> testCase = bookData.printingBooksByDesignPatterns();
        List<Book> expected = listBooks();

    }

}