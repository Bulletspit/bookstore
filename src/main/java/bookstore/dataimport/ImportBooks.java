package bookstore.dataimport;

import com.opencsv.CSVReader;
import bookstore.constructor.Author;
import bookstore.constructor.Book;
import bookstore.constructor.Category;
import bookstore.data.AuthorData;
import bookstore.data.CategoryData;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ImportBooks {

    public static List<Book> importBooksFromCSV() throws IOException {
        String file = "books.csv";

        CSVReader csvReader = new CSVReader(new FileReader(file), ';');

        List<Book> bookList = new ArrayList<>();
        String[] fileLine;
        while ((fileLine = csvReader.readNext()) != null) {
            Category categoryByID = CategoryData.getCategoryByID(Integer.valueOf(fileLine[6]));
            List<Author> authorListToAdd = new ArrayList<>();
            final String[] splitArray = fileLine[5].split(",");
            addAuthorsToList(authorListToAdd, splitArray);

            Book book = new Book(castMethodNumber(fileLine), fileLine[1], fileLine[2], castMethodYear(fileLine), castMethodCover(fileLine), authorListToAdd, categoryByID);
            bookList.add(book);
        }
        csvReader.close();
        return bookList;
    }

    private static void addAuthorsToList(List<Author> authorListToAdd, String[] splitArray) {
        for (int i = 0; i < splitArray.length; i++) {
            final Author author = AuthorData.getAuthorById(Integer.parseInt(splitArray[i]));
            authorListToAdd.add(i, author);
        }
    }

    //<editor-fold desc="casting methods">
    private static int castMethodNumber(String fileLine[]) {
        String number = fileLine[0];
        return Integer.parseInt(number);
    }

    private static int castMethodYear(String fileLine[]) {
        String year = fileLine[3];
        return Integer.parseInt(year);
    }

    private static char castMethodCover(String fileLine[]) {
        String cover = fileLine[4];
        return cover.charAt(0);
    }
    //</editor-fold>

}
