package bookstore.dataimport;

import com.opencsv.CSVReader;
import bookstore.constructor.Category;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImportCategory {
    public static List<Category> importCategoryFromFile() throws IOException {
        String file = "categories.csv";
        CSVReader csvReader = new CSVReader(new FileReader(file), ';');
        List<Category> categories = new ArrayList<Category>();
        String[] fileLine;
        while ((fileLine = csvReader.readNext()) != null) {
            String numberString = fileLine[0];
            int numberInt = Integer.parseInt(numberString);
            Category category = new Category(numberInt, fileLine[1]);
            categories.add(category);
        }
        csvReader.close();
        return categories;
    }
}
