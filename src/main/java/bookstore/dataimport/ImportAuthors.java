package bookstore.dataimport;
import com.opencsv.CSVReader;
import bookstore.constructor.Author;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImportAuthors {
    public static List<Author> importAutorsFromFile() throws IOException {
        String file = "D:\\SDA_JAVA_KURS\\programowanie2\\files\\exported\\authors.csv";
        CSVReader csvReader;
        List<Author> authorList = new ArrayList<Author>();

        csvReader = new CSVReader(new FileReader(file), ';');
        String[] fileLine;
        while ((fileLine = csvReader.readNext()) != null) {
            int idInt = castingToInt(fileLine, 0);
            int ageInt = castingToInt(fileLine, 3);
            Author author = new Author(idInt, fileLine[1], fileLine[2], ageInt);
            authorList.add(author);
        }
        csvReader.close();

        return authorList;
    }

    private static int castingToInt(String[] fileLine, int i) {
        String idString = fileLine[i];
        return Integer.parseInt(idString);
    }
}
