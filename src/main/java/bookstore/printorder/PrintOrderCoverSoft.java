package bookstore.printorder;

import bookstore.constructor.Book;

import java.util.List;
import java.util.Scanner;

public class PrintOrderCoverSoft extends PrintOrder {

    public void orderReprint(List<Book> bookList) {
        System.out.println("Choose book number");
        Scanner scanner = new Scanner(System.in);
        int choosenBookNumber = Integer.parseInt(scanner.nextLine());
        Book bookWithCover = null;
        for (Book book : bookList) {
            if (choosenBookNumber == book.getNumber()) {
                bookWithCover = book;
            }
        }
        if (bookWithCover != null) {
            if (bookWithCover.getCover() == 'M') {
                mailTo(recCover(bookWithCover));
            } else {
                System.out.println("There is not book with soft cover");
            }
        } else {
            return;
        }
    }

    @Override
    public void mailTo(Book book) {
        System.out.println("mailing: ordering book nr " + book.getNumber());
    }

    @Override
    Book recCover(Book book) {
        return null;
    }
}
