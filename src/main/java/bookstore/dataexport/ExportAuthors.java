package bookstore.dataexport;

import bookstore.constructor.Author;
import bookstore.data.AuthorData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class ExportAuthors {
    public static void writtingAuthor() throws FileNotFoundException {
        List<Author> authorList = AuthorData.getInstanceAuthors().getAuthorList();
        PrintWriter authorWriter = new PrintWriter(new File("D://SDA_JAVA_KURS//programowanie2//files//exported//authors.csv"));
        for (int i = 0; i < authorList.size(); i++) {
            final Author author = authorList.get(i);
            String sem = ";";
            authorWriter.write(author.getId() + sem + author.getName() + sem + author.getSurname() + sem + author.getAge() + "\n");
        }
        authorWriter.close();
    }


}
