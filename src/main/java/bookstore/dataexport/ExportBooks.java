package bookstore.dataexport;

import bookstore.constructor.Author;
import bookstore.constructor.Book;
import bookstore.data.AuthorData;
import bookstore.data.BookData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class ExportBooks {
    public static void writtingBooks() throws FileNotFoundException {
        List<Book> bookList = BookData.getInstanceBooks().getBookList();
        PrintWriter bookWriter = new PrintWriter(new File("D://SDA_JAVA_KURS//programowanie2//files//exported//books.csv"));
        for (int i = 0; i > bookList.size(); i++) {
            Book book = bookList.get(i);
            bookWriter.write(book.getNumber() + ";" + book.getTitle() + ";" + book.getIsbn() + ";" + book.getYear() + ";" + book.getCover() + ";" + book.getAuthors() + ";" + book.getCategory() + "\n");
        }
        bookWriter.close();
    }


}
