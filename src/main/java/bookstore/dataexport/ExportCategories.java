package bookstore.dataexport;

import bookstore.constructor.Category;
import bookstore.data.CategoryData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

public class ExportCategories {
    public static void writtingCategory() throws FileNotFoundException {
        List<Category> categoryList = CategoryData.getInstanceCategory().getCategoryList();
        PrintWriter categoryWriter = new PrintWriter(new File("D://SDA_JAVA_KURS//programowanie2//files//exported//categories.csv"));
        Category category = categoryList.get(0);

        categoryWriter.write(category.getNumber()+";"+category.getCategoryName()+"\n");
        categoryWriter.close();


    }
}
