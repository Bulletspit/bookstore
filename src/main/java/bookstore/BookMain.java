package bookstore;

import bookstore.data.AuthorData;
import bookstore.data.BookData;
import bookstore.data.CategoryData;
import bookstore.dataimport.ImportAuthors;
import bookstore.dataimport.ImportBooks;
import bookstore.dataimport.ImportCategory;
import bookstore.employees.Emploee;
import bookstore.employees.Manager;
import bookstore.employees.SalesManSlashWoman;
import bookstore.employees.Trainee;
import bookstore.functions.BookstoreMenu;

import java.io.IOException;

public class BookMain {

    public static void main(String[] args) {
        //TODO 3.  Dodanie Loggera w jednej klasie (np. zalogowanie złapanego wyjątku). Logger powinien logować do pliku i zawierać dokładną datę.
        //TODO 4.  Edycja nazwy kategorii.
        //TODO 5.  Usuwanie autora po id.

        Emploee manager = new Manager("Skiper", 35, "PL", 5648518, 20);
        Emploee salesman = new SalesManSlashWoman("Kowolski", 27, "GB", 3000);
        Emploee trainee = new Trainee("Szeregowy", 30, "CZ", 40, 6);
        System.out.println("Manager salary: " + manager.salary() + '\n' +
                "Salesman salary: " + salesman.salary() + '\n' +
                "Trainee salary: " + trainee.salary());

//        try {
//            AuthorData.getInstanceAuthors().setAuthorList(ImportAuthors.importAutorsFromFile());
//            CategoryData.getInstanceCategory().setCategoryList(ImportCategory.importCategoryFromFile());
//            BookData.getInstanceBooks().setBookList(ImportBooks.importBooksFromCSV());
//            BookstoreMenu.bookMenu();
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        }
    }


}
