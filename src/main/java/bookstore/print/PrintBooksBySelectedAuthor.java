package bookstore.print;

import bookstore.constructor.Author;
import bookstore.constructor.Book;
import bookstore.data.AuthorData;

import java.util.List;
import java.util.Scanner;

public class PrintBooksBySelectedAuthor implements PrintStrategyBooks {


    @Override
    public void print(List<Book> bookList) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please type author number to countingBooks books");
        int authorId = Integer.parseInt(scanner.nextLine());

        for (Book book : bookList) {
            for (Author author : book.getAuthors()) {
                if (authorId == author.getId()) {
                    System.out.println(book);
                }
            }
        }
    }
}
