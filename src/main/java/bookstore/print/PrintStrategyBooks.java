package bookstore.print;


import bookstore.constructor.Book;

import java.util.List;

public interface PrintStrategyBooks {
     void print(List<Book> bookList);

}
