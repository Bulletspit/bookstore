package bookstore.print;

import bookstore.constructor.Author;
import bookstore.constructor.Book;
import bookstore.data.BookData;
import com.sun.activation.registries.MailcapParseException;
import org.apache.commons.collections.map.LinkedMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class AuthorsOperations {

    public static Map countingBooks(List<Author> authorList)
    {
        int bookCount;
        Map<Author, Integer> authorbookCountMap = new HashMap<>();
        List<Book> bookList = BookData.getInstanceBooks().getBookList();
        for (Author author : authorList) {
            bookCount = 0;
            for (Book book : bookList) {
                if (book.getAuthors().contains(author)) {
                    bookCount++;
                }
            }
            authorbookCountMap.put(author, bookCount);
        }
        return authorbookCountMap;
    }
}
