package bookstore.print;

import bookstore.constructor.Author;

import java.util.List;

public class PrintAuthor implements PrintStrategyAuthor {
    @Override
    public void print(List<Author> authorList) {
        for (Author author : authorList) {
            System.out.println(author.toString());
        }
        System.out.println();
    }
}
