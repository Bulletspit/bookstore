package bookstore.print;

import bookstore.constructor.Book;

import java.util.List;

public class PrintBooksByCategoryDesignPatterns implements PrintStrategyBooks {
    @Override
    public void print(List<Book> bookList) {
        for (Book book : bookList) {
            if (book.getCategory().getNumber() == 2) {
                System.out.println(book);
            }
        }
        System.out.println();
    }
}
