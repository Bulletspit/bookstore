package bookstore.print;

import bookstore.constructor.Author;

import java.util.List;

public interface PrintStrategyAuthor {
    void print(List<Author>authorList);
}
