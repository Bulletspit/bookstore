package bookstore.print;

import bookstore.constructor.Book;

import java.util.List;

public class PrintBooks implements PrintStrategyBooks {
    @Override
    public void print(List<Book> bookList) {
        for (Book book : bookList) {
            System.out.println(book.toString());
        }
        System.out.println();
    }
}
