package bookstore.print;

import bookstore.constructor.Category;

import java.util.List;

public interface PrintSrategyCategory {
    void print(List<Category> categoryList);
}
