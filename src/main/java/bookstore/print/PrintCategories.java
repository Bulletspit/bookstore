package bookstore.print;

import bookstore.constructor.Category;

import java.util.List;

public class PrintCategories implements PrintSrategyCategory {
    @Override
    public void print(List<Category> categoryList) {
        for (Category category : categoryList) {
            System.out.println(category.toString());
        }
        System.out.println();
    }
}
