package bookstore.print;

import bookstore.constructor.Book;

import java.util.List;

public class PrintStrategyBooksYearTitleISBN implements PrintStrategyBooks {
    @Override
    public void print(List<Book> bookList) {
        for (Book book : bookList) {
            System.out.println(book.getYear() + book.getTitle() + book.getNumber());
        }
    }
}
