package bookstore.constructor;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Book {
    int number;
    String title;
    public String isbn;
    int year;
    char cover;
    List<Author> authors;
    Category category;

    public Book(int number, String title, String isbn, int year, char cover, List<Author> authors, Category category) {
        this.number = number;
        this.title = title;
        this.isbn = isbn;
        this.year = year;
        this.cover = cover;
        this.authors = authors;
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (number != book.number) return false;
        if (year != book.year) return false;
        if (cover != book.cover) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        if (isbn != null ? !isbn.equals(book.isbn) : book.isbn != null) return false;
        if (authors != null ? !authors.equals(book.authors) : book.authors != null) return false;
        return category != null ? category.equals(book.category) : book.category == null;
    }

    @Override
    public int hashCode() {
        int result = number;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (isbn != null ? isbn.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + (int) cover;
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "BOOK " + number + '{' +
                " Title : '" + title + '\'' +
                " ISBN : '" + isbn + '\'' +
                " Year : " + year + '\'' +
                " cover : " + cover + '\'' +
                " AUTHORS : " + authors + '\'' +
                " CATEGORY  : " + category +
                '}';
    }

}
