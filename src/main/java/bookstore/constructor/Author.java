package bookstore.constructor;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class Author {
    private int id;
    private String name;
    private String surname;
    private int age;

    public Author() {
    }

    public Author(int id, String name, String surname, int age) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    @Override
    public String toString() {
        return "\n'Aut. " + id +
                ", name - " + name + " " + surname +
                ", age - " + age;
    }
}
