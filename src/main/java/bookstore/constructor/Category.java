package bookstore.constructor;


public class Category {
    int number;
    String categoryName;
    int categoryPriority;

    public Category() {
    }

    public Category(int number, String categoryName) {
        this.number = number;
        this.categoryName = categoryName;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryPriority() {
        return categoryPriority;
    }

    public void setCategoryPriority(int categoryPriority) {
        this.categoryPriority = categoryPriority;
    }

    @Override
    public String toString() {
        return "Cat. " + number +
                " { : " + categoryName + '\'' +
                '}';
    }
}
