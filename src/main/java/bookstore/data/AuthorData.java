package bookstore.data;

import bookstore.constructor.Author;
import bookstore.constructor.Book;

import java.util.ArrayList;
import java.util.List;

public class AuthorData {
    public static AuthorData INSTANCE;

    protected AuthorData() {
    }

    public static List<Author> authorList = new ArrayList<Author>();

    public void setAuthorList(List<Author> authors) {
        this.authorList = authors;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public static AuthorData getInstanceAuthors() {
        if (INSTANCE == null) {
            INSTANCE = new AuthorData();
        }
        return INSTANCE;
    }

    public static Author getAuthorById(int authorId) {
        return authorList.stream().filter(author -> author.getId() == authorId).findFirst().get();
    }


}
