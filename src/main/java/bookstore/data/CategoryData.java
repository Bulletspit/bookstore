package bookstore.data;

import bookstore.constructor.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryData {
    private static CategoryData INSTANCE;

    protected CategoryData() {
    }

    private static List<Category> categoryList = new ArrayList<Category>();

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public static CategoryData getInstanceCategory() {
        if (INSTANCE == null) {
            INSTANCE = new CategoryData();
        }
        return INSTANCE;
    }

    static void printCategory() {
        for (Category category : categoryList) {
            System.out.println(category.toString());
        }
        System.out.println();
    }

    public static Category getCategoryByID(int id) {
        Category category = new Category();
        for (Category item : categoryList) {
            if (item.getNumber() == id) {
                category = item;
            }
        }
        return category;
    }
}
