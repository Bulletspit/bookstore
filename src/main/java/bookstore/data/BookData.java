package bookstore.data;

import bookstore.constructor.Book;

import java.util.ArrayList;
import java.util.List;


public class BookData {
    public static BookData INSTANCE;

    public BookData() {
    }

    public static List<Book> bookList = new ArrayList<>();

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public static BookData getInstanceBooks() {
        if (INSTANCE == null) {
            INSTANCE = new BookData();
        }
        return INSTANCE;
    }
}
