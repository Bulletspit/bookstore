package bookstore.employees;

public class SalesManSlashWoman extends Emploee {
    int salary;

    public SalesManSlashWoman(String surname, int age, String countryCode, int salary) {
        super(surname, age, countryCode);
        this.salary = salary;
    }

    @Override
    public int salary() {
        return salary;
    }
}
