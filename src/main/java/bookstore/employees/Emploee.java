package bookstore.employees;

public abstract class Emploee {
    String surname;
    int age;
    String countryCode;

    public Emploee(String surname, int age, String countryCode) {
        this.surname = surname;
        this.age = age;
        this.countryCode = countryCode;
    }

    public abstract int salary();
}
