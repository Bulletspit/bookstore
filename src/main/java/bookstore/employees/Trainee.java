package bookstore.employees;

public class Trainee extends Emploee {
    int workHoursMonthly;
    int hourlySalary;

    public Trainee(String surname, int age, String countryCode, int workHoursMonthly, int hourlySalary) {
        super(surname, age, countryCode);
        this.workHoursMonthly = workHoursMonthly;
        this.hourlySalary = hourlySalary;
    }

    @Override
    public int salary() {
        return workHoursMonthly * hourlySalary;
    }
}
