package bookstore.employees;

public class Manager extends Emploee {
    int phoneNumber;
    int b2b;

    public Manager(String surname, int age, String countryCode, int phoneNumber, int b2b) {
        super(surname, age, countryCode);
        this.phoneNumber = phoneNumber;
        this.b2b = b2b;
    }

    @Override
    public int salary() {
        return 20 * b2b * 8;
    }
}
