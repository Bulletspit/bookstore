package bookstore.functions;

import bookstore.constructor.Book;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BookFunctions {

    Book findByISBN(List<Book> books, String isbn) {
        Book book = null;
        for (Book item : books) {
            if (item.isbn.equals(isbn)) {
                book = item;
            }
        }
        return book;
    }

    Book findByISBNStreamStyle(List<Book> bookList, String isbn) {

        return bookList
                .stream()
                .filter(book1 -> book1.isbn.equals(isbn))
                .findFirst()
                .get();

    }

    int yaerSum(List<Book> books) {
        int sum = 0;
        for (Book book : books) {
            final int year = book.getYear();
            sum += year;
        }
        return sum;
    }

    int yaerSumStreamStyle(List<Book> listBooks) {

        return listBooks.stream()
                .filter(i -> i.getYear() > 0)
                .mapToInt(Book::getYear).sum();
    }

    int bookCountAfter2007(List<Book> books) {
        int counter = 0;
        for (Book item : books) {
            if (item.getYear() > 2007) {
                counter++;
            }
        }
        return counter;
    }

    int bookCountAfter2007StreamStyle(List<Book> listBooks) {
        return (int) listBooks
                .stream()
                .filter(i -> i.getYear() > 2007)
                .count();
    }


    boolean isAfter2000(List<Book> listBooks) {
        boolean isAfter2000 = false;
        for (Book book : listBooks) {
            if (book.getYear() > 2000) {
                isAfter2000 = true;
            }
        }
        return isAfter2000;
    }

    boolean isAfter2000StreamStyle(List<Book> listBooks) {
        return listBooks
                .stream()
                .allMatch(book -> book.getYear() > 2000);
    }

    List<Book> giveMeTwoLastBooks(List<Book> listBooks) {
        List<Book> twoBooks;
        twoBooks = listBooks.subList(4, 6);
        return twoBooks;
    }

    List<Book> giveMeTwoLastBooksStreamStyle(List<Book> listBooks) {
        return listBooks
                .stream()
                .skip(listBooks.size() - 2)
                .collect(Collectors.toList());
    }

    public Book givingEarliestRelease(List<Book> listBooks) {
        Book book = null;
        int i = 2018;
        for (Book item : listBooks) {
            if (item.getYear() < i) {
                i = item.getYear();
                book = item;
            }
        }
        return book;
    }

//    Book givingEarliestReleaseStreamStyle(List<Book> listBooks) {
//        return listBooks
//                .stream()
//                .min(new bookstore.functions.YearCompoarator())
//                .get();
//    }

    public List<Book> givingLatestrelease(List<Book> listBooks) {
        List<Book> latestRelease = new ArrayList<Book>();
        int i = 0;
        for (Book book : listBooks) {
            if (book.getYear() > i) {
                i = book.getYear();
                latestRelease.add(book);
            }
        }
        return latestRelease;
    }

    public List<Book> givingLatestreleaseStreamStyle(List<Book> listBooks) {
        return listBooks.stream().filter(book -> book.getYear() > 0).collect(Collectors.toList());
    }

    public double givingAverageYearRelease(List<Book> listBooks) {
        double averageYearRelease = 0.0;
        for (Book book : listBooks) {
            double yearValue = book.getYear();
            averageYearRelease += yearValue / listBooks.size();
        }
        return averageYearRelease;
    }

    public double givingAverageYearReleaseStreamStyle(List<Book> listBooks) {

        return listBooks.stream().mapToDouble(Book::getYear).average().getAsDouble();
    }

    boolean checkIfReleasedBefore2003(List<Book> listBooks) {
        boolean releasedBefore20013 = false;
        for (Book book : listBooks) {
            final int checkYear = book.getYear();
            if (checkYear < 2003) {
                releasedBefore20013 = true;
            }
        }
        return releasedBefore20013;
    }

    boolean checkIfReleasedBefore2003StreamStyle(List<Book> listBooks) {
        return listBooks.stream().anyMatch(book -> book.getYear() < 2003);
    }

    List<Book> givingBooksStartsWithCAndReleasedAfter2010(List<Book> listBooks) {
        final ArrayList<Book> booksReturned = new ArrayList<Book>();
        for (Book book : listBooks) {
            if (book.getYear() > 2010 && book.toString().contains("c")) {
                booksReturned.add(book);
            }
        }
        return booksReturned;
    }

    List<Book> givingBooksStartsWithCAndReleasedAfter2010streamStyle(List<Book> listBooks) {
        return listBooks
                .stream()
                .filter(book -> book.getTitle().contains("c") && book.getYear() > 2010)
                .collect(Collectors.toList());
    }

//    List<Book> returnSortedBookListByYearAscending(List<Book> listBooks) {
//        Collections.sort(listBooks, new bookstore.functions.YearCompoarator());
//        return listBooks;
//    }
//
//    List<Book> returnSortedBookListByYearAscendingStreamStyle(List<Book> listBooks) {
//        return listBooks.stream().sorted(new bookstore.functions.YearCompoarator()).collect(Collectors.toList());
//    }
//
//    List<Book> returnSortedBookListByYearDescending(List<Book> listBooks) {
//        Collections.sort(listBooks, new bookstore.functions.YearCompoarator());
//        Collections.reverse(listBooks);
//        return listBooks;
//    }
//
//    List<Book> returnSortedBookListByYearDescendingStreamStyle(List<Book> listBooks) {
//        return listBooks
//                .stream()
//                .sorted(new bookstore.functions.YearCompoarator()
//                        .reversed())
//                .collect(Collectors.toList());
//    }

    List<Book> adding100YearsToReleaseDate(List<Book> listBooks) {

        for (Book book : listBooks) {
            final int incrementYear = book.getYear() + 100;
            book.setYear(incrementYear);
        }
        return listBooks;
    }

    List<Book> adding100YearsToReleaseDateStreamStyle(List<Book> listBooks) {
        return listBooks
                .stream()
                .map(book -> new Book(book.getNumber(), book.getTitle(), book.getIsbn(), book.getYear() + 100, book.getCover(), book.getAuthors(), book.getCategory()))
                .collect(Collectors.toList());
    }

    List<Book> booksDivisibleByTwo(List<Book> listBooks) {
        List<Book> divisible = new ArrayList<>();
        for (Book book : listBooks) {
            if (book.getYear() % 2 == 0) {
                divisible.add(book);
            }
        }
        return divisible;
    }

    List<Book> booksDivisibleByTwoStreamStyle(List<Book> listBooks) {
        return listBooks.stream().filter(book -> book.getYear() % 2 == 0).collect(Collectors.toList());
    }

    List<Book> sortingAlphabetically(List<Book> listBooks) {
        listBooks.sort(new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });
        return listBooks;
    }

    List<Book> sortingAlphabeticallyStreamStyle(List<Book> listBooks) {
        return listBooks
                .stream()
                .sorted((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()))
                .collect(Collectors.toList());
    }
}
