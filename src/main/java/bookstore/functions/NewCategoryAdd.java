package bookstore.functions;

import bookstore.constructor.Category;
import bookstore.data.CategoryData;

import java.util.Scanner;

public class NewCategoryAdd {
    public static void addingdNewCategory() {
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            System.out.println("Set category number");
            int number = Integer.parseInt(scanner.nextLine());
            System.out.println("Set category name");
            String categoryName = scanner.nextLine();
            final Category category = new Category(number, categoryName);
            boolean exist = false;
            isExist(number, category, exist);
            subMenuPrint();
            int exit = scanner.nextInt();
            if (subMenuMethod(exit)) break;
        }
    }

    private static boolean subMenuMethod(int exit) {
        if (exit == 0) {
            return true;
        } else if (exit == 1) {
            // continue;
        }
        return false;
    }

    private static void isExist(int number, Category category, boolean exist) {
        for (Category categoryItem : CategoryData.getInstanceCategory().getCategoryList()) {
            if (categoryItem.getNumber() == category.getNumber()) {
                exist = true;
                System.out.println("bookstore.constructor.Category with id " + number + " exist");
            }
        }
        if (!exist) {
            CategoryData.getInstanceCategory().getCategoryList().add(category);
        }
    }

    private static void subMenuPrint() {
        System.out.println("press 0 to back to main menu ");
        System.out.println("press 1 to add new category ");
    }


}
