package bookstore.functions;

import bookstore.constructor.Book;
import bookstore.data.BookData;

import java.util.List;
import java.util.Scanner;

public class EditBookClass {
    public static void bookEditing() {
        List<Book> bookList = BookData.getInstanceBooks().getBookList();
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Choose book id:");
        int id = Integer.parseInt(scanner.nextLine());
        boolean isExist = isExist(bookList, id);
        System.out.println(isExist ? "editing book number " + id : " does not exist");
        editTitle(bookList, scanner, id, isExist);
    }

    private static void editTitle(List<Book> bookList, Scanner scanner, int id, boolean isExist) {
        if (isExist) {
            System.out.println("Edit title");
            String edit = scanner.nextLine();
            setTitle(bookList, id, edit);
        }
    }

    private static void setTitle(List<Book> bookList, int id, String edit) {
        for (Book item : bookList) {
            if (item.getNumber() == id) {
                item.setTitle(edit);
            }
            break;
        }
    }

    private static boolean isExist(List<Book> bookList, int id) {
        for (Book item : bookList) {
            if (item.getNumber() == id) {
                return true;
            }
        }
        return false;
    }


}
