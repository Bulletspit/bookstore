package bookstore.functions;

import bookstore.constructor.Author;
import bookstore.data.AuthorData;

import java.util.List;
import java.util.Scanner;

public class EditAuthorClass {
    public static void authorEditing() {
        List<Author> authorList = AuthorData.getInstanceAuthors().getAuthorList();
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Choose author id:");
        int id = Integer.parseInt(scanner.nextLine());
        boolean isExist = isExist(authorList, id);
        System.out.println(isExist ? "editing author with number " + id : " does not exist");
        editAge(authorList, scanner, id, isExist);
    }

    private static void editAge(List<Author> authorList, Scanner scanner, int id, boolean isExist) {
        if (isExist) {
            System.out.println("Edit authors age");
            int editAge = Integer.parseInt(scanner.nextLine());
            setTitle(authorList, id, editAge);
        }
    }

    private static void setTitle(List<Author> authorList, int id, int editAge) {
        for (Author item : authorList) {
            if (item.getId() == id) {
                item.setAge(editAge);
            }
            break;
        }
    }

    private static boolean isExist(List<Author> authorList, int id) {
        for (Author item : authorList) {
            if (item.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
