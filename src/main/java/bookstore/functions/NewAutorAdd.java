package bookstore.functions;

import bookstore.constructor.Author;
import bookstore.data.AuthorData;

import java.util.Scanner;

public class NewAutorAdd {


    public static void addingNewAuthors() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Id: ");
            int id = Integer.parseInt(scanner.nextLine());
            System.out.println("Name: ");
            String name = scanner.nextLine();
            System.out.println("Surname: ");
            String surname = scanner.nextLine();
            System.out.println();
            System.out.println("Age: ");
            int age = scanner.nextInt();
            Author author = new Author(id, name, surname, age);
            boolean exist = false;
            isExist(id, author, exist);
            print();
            int exit = scanner.nextInt();
            if (subMenuMethod(exit)) break;
        }
    }

    private static boolean subMenuMethod(int exit) {
        return exit == 0;
    }

    private static void isExist(int id, Author author, boolean exist) {
        for (Author instanceAuthor : AuthorData.getInstanceAuthors().getAuthorList()) {
            if (instanceAuthor.getId() == author.getId()) {
                System.out.println("bookstore.constructor.Author with that id: " + id + " exist");
                exist = true;
                break;
            }
        }
        if (!exist) {
            AuthorData.getInstanceAuthors().getAuthorList().add(author);
        }
    }

    static void print() {
        System.out.println("press 0 to back to main menu ");
        System.out.println("press 1 to add new author ");
    }
}
