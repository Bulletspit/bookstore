package bookstore.functions;

import bookstore.constructor.Author;
import bookstore.constructor.Book;
import bookstore.constructor.Category;
import bookstore.data.AuthorData;
import bookstore.data.BookData;
import bookstore.data.CategoryData;
import bookstore.dataexport.ExportAuthors;
import bookstore.dataexport.ExportBooks;
import bookstore.dataexport.ExportCategories;
import bookstore.print.*;
import bookstore.printorder.PrintOrder;
import bookstore.printorder.PrintOrderCoverSoft;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class BookstoreMenu {
    private static PrintStrategyBooks printStrategyBooks = new PrintBooks();
    private static PrintSrategyCategory printSrategyCategory = new PrintCategories();
    private static PrintStrategyAuthor printStrategyAuthor = new PrintAuthor();
    private static PrintOrder printOrder = new PrintOrderCoverSoft();


    public static void bookMenu() throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        boolean status = true;
        System.out.println("*****************************************************************************" +
                "\n*                                                                           *" +
                "\n*!!!!!!!!!!!!!   WELCOME IN THE LARGEST LIBRARY IN THE WORLD   !!!!!!!!!!!!!*" +
                "\n*                                                                           *" +
                "\n*****************************************************************************");
        final List<Book> bookList = BookData.getInstanceBooks().getBookList();
        final List<Category> categoryList = CategoryData.getInstanceCategory().getCategoryList();
        final List<Author> authorList = AuthorData.getInstanceAuthors().getAuthorList();
        while (status) {
            printMenu();
            int chooseNumber = Integer.parseInt(scanner.nextLine());
            switch (chooseNumber) {
                case 0:
                    System.out.println("!!!!!!!!!!!!!!!!!!!   GOOD BYE   !!!!!!!!!!!!!!!!!!!");
                    status = false;
                    break;
                case 1:
                    System.out.println(chooseNumber + " : bookstore@books.com");
                    break;
                case 2:
                    printStrategyBooks.print(bookList);
                    break;
                case 3:
                    printSrategyCategory.print(categoryList);
                    break;
                case 4:
                    printStrategyAuthor.print(authorList);
                    break;
                case 5:
                    printStrategyBooks = new PrintBooksByCategoryDesignPatterns();
                    printStrategyBooks.print(bookList);
                    break;
                case 6:
                    NewAutorAdd.addingNewAuthors();
                    break;
                case 7:
                    NewCategoryAdd.addingdNewCategory();
                    break;
                case 8:
                    EditBookClass.bookEditing();
                    break;
                case 9:
                    printStrategyBooks = new PrintStrategyBooksYearTitleISBN();
                    printStrategyBooks.print(bookList);

                    break;
                case 10:
                    ExportAuthors.writtingAuthor();
                    break;
                case 11:
                    ExportCategories.writtingCategory();
                    break;
                case 12:
                    ExportBooks.writtingBooks();
                    break;
                case 13:
                    ExportAuthors.writtingAuthor();
                    ExportCategories.writtingCategory();
                    ExportBooks.writtingBooks();
                    break;
                case 14:
                    EditAuthorClass.authorEditing();
                    break;
                case 15:
                    printStrategyBooks = new PrintBooksBySelectedAuthor();
                    printStrategyBooks.print(bookList);
                    break;
                case 16:
                    System.out.println(AuthorsOperations.countingBooks(authorList));
                    break;
                case 17:
                   // printOrder.order(bookList);
                    break;
                default:
                    printDefault();
                    break;
            }

        }
    }

    private static void printDefault() {
        System.out.println("\n" +
                "\n" +
                "\n" +
                "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    WRONG NUMBER    !!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
                "\n" +
                "\n");
    }

    private static void printMenu() {
        System.out.println("Menu:\n" +
                "0. EXIT\n" +
                "1. Contact\n" +
                "2. Print book list \n" +
                "3. Print categories\n" +
                "4. Print authors\n" +
                "5. Print books by category: Design Patterns \n" +
                "6. Add author\n" +
                "7. Add category\n" +
                "8. Edit book title\n" +
                "9. Print books by Year\n" +
                "10. Eport authors to file\n" +
                "11. Eport categoris to file\n" +
                "12. Export books to file\n" +
                "13. Save status\n" +
                "14. Edit authors age\n" +
                "15. Print books by selected author\n" +
                "16. Priny auhors with book counts\n" +
                "17. Print cover test");

    }
}
